	LIST P=18F4321, F=INHX32
	#include <P18F4321.INC>	

;******************
;* CONFIGURACIONS *
;******************
	CONFIG	OSC = HS             
	CONFIG	PBADEN = DIG
	CONFIG	WDT = OFF

;*************
;* VARIABLES *
;*************
	HAY_MENSAJE EQU 0x00
	AUX EQU 0x01
	BARRIDO EQU 0x02
	SENTIDO EQU 0x03
	ROTADOS EQU 0x04
	TIEMPOL EQU 0x05
	TIEMPOH EQU 0x06
	NCHARSL EQU 0x07
	NCHARSH EQU 0x08
	ENVIADOS EQU 0x09
 	BLINKING_5HZ EQU 0x0A
	BLINKING_10HZ EQU 0x0B
 
;*************
;* CONSTANTS *
;*************
	MENSAJE_INICIO EQU 0xD0
	PETICION EQU 0x41

;*********************************
; VECTORS DE RESET I INTERRUPCI? *
;*********************************
	
	ORG 0x000000
RESET_VECTOR
	goto MAIN		

	ORG 0x000008
HI_INT_VECTOR
	goto	HIGH_INT	

	ORG 0x000018
LOW_INT_VECTOR
	retfie FAST		

;**********
;* TABLAS *
;**********
	
	;tabla donde se guarda cada cuantos bits hay que encender un nuevo led
	;de la barra de progreso
	ORG 0x000020
	DB 0x00, 0x00
	DB 0x00, 0x00
	DB 0x00, 0x00
	DB 0x00, 0x00
	DB 0x00, 0x00
	DB 0x01, 0x01
	DB 0x01, 0x01
	DB 0x01, 0x01
	DB 0x01, 0x01
	DB 0x01, 0x01
	DB 0x02, 0x02
	DB 0x02, 0x02
	DB 0x02, 0x02
	DB 0x02, 0x02
	DB 0x02, 0x02
	DB 0x03, 0x03
	DB 0x03, 0x03
	DB 0x03, 0x03
	DB 0x03, 0x03
	DB 0x03, 0x03
	DB 0x04, 0x04
	DB 0x04, 0x04
	DB 0x04, 0x04
	DB 0x04, 0x04
	DB 0x04, 0x04
	DB 0x05, 0x05
	DB 0x05, 0x05
	DB 0x05, 0x05
	DB 0x05, 0x05
	DB 0x05, 0x05
	DB 0x06, 0x06
	DB 0x06, 0x06
	DB 0x06, 0x06
	DB 0x06, 0x06
	DB 0x06, 0x06
	DB 0x07, 0x07
	DB 0x07, 0x07
	DB 0x07, 0x07
	DB 0x07, 0x07
	DB 0x07, 0x07
	DB 0x08, 0x08
	DB 0x08, 0x08
	DB 0x08, 0x08
	DB 0x08, 0x08
	DB 0x08, 0x08
	DB 0x09, 0x09
	DB 0x09, 0x09
	DB 0x09, 0x09
	DB 0x09, 0x09
	DB 0x09, 0x09
	DB 0x0A, 0x0A
	DB 0x0A, 0x0A
	DB 0x0A, 0x0A
	DB 0x0A, 0x0A
	DB 0x0A, 0x0A
	DB 0x0B, 0x0B
	DB 0x0B, 0x0B
	DB 0x0B, 0x0B
	DB 0x0B, 0x0B
	DB 0x0B, 0x0B
	DB 0x0C, 0x0C
	DB 0x0C, 0x0C
	DB 0x0C, 0x0C
	DB 0x0C, 0x0C
	DB 0x0C, 0x0C
	DB 0x0D, 0x0D
	DB 0x0D, 0x0D
	DB 0x0D, 0x0D
	DB 0x0D, 0x0D
	DB 0x0D, 0x0D
	DB 0x0E, 0x0E
	DB 0x0E, 0x0E
	DB 0x0E, 0x0E
	DB 0x0E, 0x0E
	DB 0x0E, 0x0E
	DB 0x0F, 0x0F
	DB 0x0F, 0x0F
	DB 0x0F, 0x0F
	DB 0x0F, 0x0F
	DB 0x0F, 0x0F
	DB 0x10, 0x10
	DB 0x10, 0x10
	DB 0x10, 0x10
	DB 0x10, 0x10
	DB 0x10, 0x10
	DB 0x11, 0x11
	DB 0x11, 0x11
	DB 0x11, 0x11
	DB 0x11, 0x11
	DB 0x11, 0x11
	DB 0x12, 0x12
	DB 0x12, 0x12
	DB 0x12, 0x12
	DB 0x12, 0x12
	DB 0x12, 0x12
	DB 0x13, 0x13
	DB 0x13, 0x13
	DB 0x13, 0x13
	DB 0x13, 0x13
	DB 0x13, 0x13
	DB 0x14, 0x14
	DB 0x14, 0x14
	DB 0x14, 0x14
	DB 0x14, 0x14
	DB 0x14, 0x14
	DB 0x15, 0x15
	DB 0x15, 0x15
	DB 0x15, 0x15
	DB 0x15, 0x15
	DB 0x15, 0x15
	DB 0x16, 0x16
	DB 0x16, 0x16
	DB 0x16, 0x16
	DB 0x16, 0x16
	DB 0x16, 0x16
	DB 0x17, 0x17
	DB 0x17, 0x17
	DB 0x17, 0x17
	DB 0x17, 0x17
	DB 0x17, 0x17
	DB 0x18, 0x18
	DB 0x18, 0x18
	DB 0x18, 0x18
	DB 0x18, 0x18
	DB 0x18, 0x18
	DB 0x19, 0x19
	DB 0x19, 0x19
	DB 0x19, 0x19
	DB 0x19, 0x19
	DB 0x19, 0x19
	DB 0x1A, 0x1A
	DB 0x1A, 0x1A
	DB 0x1A, 0x1A
	DB 0x1A, 0x1A
	DB 0x1A, 0x1A
	DB 0x1B, 0x1B
	DB 0x1B, 0x1B
	DB 0x1B, 0x1B
	DB 0x1B, 0x1B
	DB 0x1B, 0x1B
	DB 0x1C, 0x1C
	DB 0x1C, 0x1C
	DB 0x1C, 0x1C
	DB 0x1C, 0x1C
	DB 0x1C, 0x1C
	DB 0x1D, 0x1D
	DB 0x1D, 0x1D
	DB 0x1D, 0x1D
	DB 0x1D, 0x1D
	DB 0x1D, 0x1D
	DB 0x1E, 0x1E
	DB 0x1E, 0x1E
	DB 0x1E, 0x1E
	DB 0x1E, 0x1E
	DB 0x1E, 0x1E

;***********************************
;* RUTINES DE SERVEI D'INTERRUPCIO *
;***********************************
	
HIGH_INT
	call RESET_TIMER
	incf TIEMPOL, 1, 0
	btfsc STATUS, C, 0
	incf TIEMPOH, 1, 0
	retfie	FAST

;****************************
;* MAIN I RESTA DE FUNCIONS *
;****************************
	
INIT_VARS
	clrf HAY_MENSAJE, 0
	clrf BARRIDO, 0
	clrf PETICION, 0
	clrf TIEMPOL, 0
	clrf TIEMPOH, 0
	clrf SENTIDO, 0
	clrf NCHARSL, 0
	clrf NCHARSH, 0
	clrf ENVIADOS, 0
	clrf BLINKING_5HZ, 0
	clrf BLINKING_10HZ, 0
	return
	
INIT_PORTS
	clrf TRISD, 0
	movlw 0x08 ;00001000
	movwf TRISE, 0
	movlw 0x03 ;00000011
	movwf TRISB, 0
	bcf TRISC, 0, 0
	bsf TRISC, 6, 0
	bsf TRISC, 7, 0
	bsf LATC, 0, 0
	clrf LATD, 0
	clrf LATE, 0
	return
	
INIT_EUSART
	movlw 0x26 ;00100010
	movwf TXSTA, 0
	movlw 0x90 ;10010000
	movwf RCSTA, 0
	clrf BAUDCON, 0
	movlw 0x1F ;00011111
	movwf SPBRG, 0
	return
	
INIT_TIMER
	bcf RCON, IPEN, 0
	movlw 0x88 ;10001000
	movwf T0CON, 0
	movlw 0xE0 ;11100000
	movwf INTCON, 0
	bcf INTCON, TMR0IF, 0
	call RESET_TIMER
	return
	
RESET_TIMER
	movlw 0xCF ;10111111
	movwf TMR0H, 0
	movlw 0x30 ;00110000
	movwf TMR0L, 0
	bcf INTCON, TMR0IF, 0
	return
	
;********
;* MAIN *
;********

MAIN
	call INIT_PORTS
	call INIT_EUSART
	call INIT_TIMER
	call INIT_VARS
	clrf TBLPTRU, 0 ;nunca se utiliza, y lo ponemos a 0 para poder jugar con TBLPTRH y L
	
LOOP
	;recibi algo?
	btfsc PIR1, RCIF, 0 
	call REBUT
	
	;me han apretado un pulsador?
	btfsc PORTB, 0, 0
	call PULSADOR_TX
	btfsc PORTB, 1, 0
	call PULSADOR_RX
	
	;hay que hacer barrido?
	btfsc BARRIDO, 0, 0
	call HACER_BARRIDO
	
	;hay que hacer blinking?
	btfsc BLINKING_10HZ, 0, 0
	call BLINKING_10
	btfsc BLINKING_5HZ, 0, 0
	call BLINKING_5
	
	goto LOOP
	
;********
;* FUNCS *
;********	
	
ESPERA_ENVIAMIENTO
	btfss TXSTA, TRMT, 0
	goto ESPERA_ENVIAMIENTO
	return	
	
REBUT
	;reseteamos los LEDS y comprobamos lo que hemos recibido
	call RESET_LEDS
	movff RCREG, AUX
	movlw 0x2E ;'.'
	cpfseq AUX, 0
	;tenemos que comprobar si tenemos que enviar por RF
	goto IF_ELSE 
	;tenemos que cargar un mensaje en la RAM
	goto CARGAR_MENSAJE
	
IF_ELSE 
	;para tratar con el mensaje, primero hay que comprobar si hay mensaje cargado en la RAM
	btfss HAY_MENSAJE, 0, 0 
	goto ACTIVA_BARRIDO
	movlw 0x44 ;'D' de Dani
	cpfseq AUX, 0
	goto DEVOLVER_MENSAJE ;si se recibio cualquier otra cosa, devolver el mensaje almacenado en la RAM por SIO
	goto ENVIA_RF ;enviarlo por RF
	
CARGAR_MENSAJE
	;nos pondremos en el punto de la RAM donde empezaremos a cargar el mensaje
	call PREPARA_PUNTEROS
	clrf NCHARSL, 0 ;poner el contador de caracteres que tiene el mensaje a 0
	clrf NCHARSH, 0
WAIT
	;esperamos a recibir algo de la SIO
	btfss PIR1, RCIF, 0
	goto WAIT
	;aqui se recibio algo
	movff RCREG, AUX
	movff AUX, POSTINC0 
	;aumentamos el numero de caracteres recibidos
	incf NCHARSL, 1, 0
	btfsc STATUS, C, 0
	incf NCHARSH, 1, 0
	;comparar que sea (o no) el caracter que marca el final de mensaje
	movlw 0x00
	cpfseq AUX, 0
	goto WAIT ;esperar siguiente caracter
	;si ya hemos acacado activaremos el flag que indica que hay un mensaje
	setf HAY_MENSAJE, 0
	decf NCHARSL, 1, 0 ;descontar en 1 el contador de caracteres, ya que tambien se conto el char que marca el final
	btfss STATUS, C, 0
	decf NCHARSH, 1, 0
	;"dividiremos" el numero de caracteres ente 10
	call DIVIDIR_10
	setf BLINKING_5HZ ;activar el flag de que ahora se tiene que hacer blinking a 5Hz
	return
	
;devolveremos el mensaje cargado en la PIC por canal serie, esta funcion es unicamente para debuggar
;no tiene ningun otro fin aparte de ello
DEVOLVER_MENSAJE 
	call PREPARA_PUNTEROS
CONTINUE
	movff POSTINC0, AUX
	movlw 0x00
	cpfseq AUX, 0
	goto DEVOLVER_CARACTER
	return
DEVOLVER_CARACTER
	movff AUX, TXREG
	call ESPERA_ENVIAMIENTO
	goto CONTINUE
	
;nos situamos en el punto de la RAM donde empieza el mensaje	
PREPARA_PUNTEROS
	clrf FSR0H, 0
	movlw MENSAJE_INICIO
	movwf FSR0L, 0
	return
	
PULSADOR_TX
	call FILTRA_REBOTES
	;si despues de filtrar sigue estando activo, entonces haremos la transmision
	btfss PORTB, 0, 0
	return	
	call RESET_LEDS ;apagamos los leds ignorando el estado en el que estaban antes, ya que tenemos que hacer otras cosas ahora
;esperamos a que el pulsador baje
ESPERA_0_TX
	btfsc PORTB, 0, 0
	goto ESPERA_0_TX
	call FILTRA_REBOTES
	;si no hay mensaje cargado en la PIC -> coche fantastico
	btfss HAY_MENSAJE, 0, 0
	goto ACTIVA_BARRIDO
	;si hay mensaje, lo enviaremos por RF
	call ENVIA_RF
	return
	
;esperamos entre 10 y 15 ms	
FILTRA_REBOTES
	clrf TIEMPOL, 0
	clrf TIEMPOH, 0
INNER
	movlw 0x02
	cpfsgt TIEMPOL, 0
	goto INNER
	return
	
;funcion para poner un "valor" inicial en los LEDs y que comienze a hacer coche fantastico
ACTIVA_BARRIDO
	movlw 0x03
	movwf LATD, 0
	setf BARRIDO, 0
	clrf ROTADOS, 0
	clrf SENTIDO, 0
	return
		
HACER_BARRIDO
	;cuando pasen 40ms rotaremos los LEDS
	movlw 0x08
	cpfsgt TIEMPOL, 0
	return
	clrf TIEMPOL, 0
	bcf STATUS, C, 0
	;dependiendo del sentido iremos rotaremos en una direccion u otra
	btfsc SENTIDO, 0, 0
	goto BARRIDO_DERECHA
	goto BARRIDO_IZQUIERDA
	return ;por educacion
	
BARRIDO_IZQUIERDA
	rlcf LATD, 1, 0
	rlcf LATE, 1, 0
	incf ROTADOS, 1, 0
	movlw 0x07
	cpfsgt ROTADOS, 0
	return
	setf SENTIDO, 0
	clrf ROTADOS, 0
	return
	
BARRIDO_DERECHA
	nop ;?? obsesivo compulsivo de que los caminos no son iguales
	rrcf LATE, 1, 0
	rrcf LATD, 1, 0
	incf ROTADOS, 1, 0
	movlw 0x07                         
	cpfsgt ROTADOS, 0
	return
	clrf SENTIDO, 0
	clrf ROTADOS, 0
	return
	
RESET_LEDS
	clrf LATD, 0
	clrf LATE, 0
	clrf BARRIDO, 0
	clrf BLINKING_5HZ, 0
	clrf BLINKING_10HZ, 0
	return
	
PULSADOR_RX
	call FILTRA_REBOTES
	;si despues de filtrar sigue estando activo, entonces pediremos mensaje
	btfss PORTB, 1, 0
	return	
	call RESET_LEDS
;esperamos a que el pulsador baje
ESPERA_0_RX
	btfsc PORTB, 1, 0
	goto ESPERA_0_RX
	call FILTRA_REBOTES
	movlw PETICION ;'A' de Alejandra
	;le enviamos la peticion al ordenador y esperamos una respuesta
	movwf TXREG, 0 
	call ESPERA_ENVIAMIENTO
	call ESPERA_10S
	return
	
ESPERA_10S
	clrf TIEMPOL, 0
	clrf TIEMPOH, 0
CONTINUA10
	btfsc PIR1, RCIF, 0 
	goto REBUT ;llego algo, irnos de aqui
	movlw 0x07
	cpfseq TIEMPOH, 0
	goto CONTINUA10
	movlw 0xD0
	cpfseq TIEMPOL, 0
	goto CONTINUA10
	goto ACTIVA_BARRIDO
	return ;por educacion

;accedemos a la tabla con las divisiones en funcion del numero de caracteres del mensaje
DIVIDIR_10
	movff NCHARSL, TBLPTRL
	movff NCHARSH, TBLPTRH
	movlw 0x20
	addwf TBLPTRL, 1, 0
	btfsc STATUS, C, 0
	incf TBLPTRH, 1, 0
	;dejamos el valor de la division en el TABLAT
	tblrd*
	return

ENVIA_RF
	;nos situamos al inicio del mensaje
	call PREPARA_PUNTEROS
	clrf TIEMPOL, 0
	clrf ENVIADOS, 0
	movlw 0x00
	cpfsgt TABLAT, 0
	;si el mensaje tiene menos de 10 caracteres se encendera todo de golpe
	call ENCENDER_TODO
	call RESET_TIMER
	bcf LATC, 0, 0
;ponemos la salida del RF a '0' durante 20ms	
WAIT_20MS
	btfss TIEMPOL, 2, 0
	goto WAIT_20MS
	;inicio byte identificador nuestro
	call ENVIA_0
	call ENVIA_1
	call ENVIA_1
	call ENVIA_1
	call ENVIA_0
	call ENVIA_0
	call ENVIA_1
	call ENVIA_1
;enviamos lo que haya en el tablat bit a bit	
SIGUE_ENVIANDO	
	btfss INDF0, 7, 0
	call ENVIA_0
	btfsc INDF0, 7, 0
	call ENVIA_1
	btfss INDF0, 6, 0
	call ENVIA_0
	btfsc INDF0, 6, 0
	call ENVIA_1
	btfss INDF0, 5, 0
	call ENVIA_0
	btfsc INDF0, 5, 0
	call ENVIA_1
	btfss INDF0, 4, 0
	call ENVIA_0
	btfsc INDF0, 4, 0
	call ENVIA_1
	btfss INDF0, 3, 0
	call ENVIA_0
	btfsc INDF0, 3, 0
	call ENVIA_1
	btfss INDF0, 2, 0
	call ENVIA_0
	btfsc INDF0, 2, 0
	call ENVIA_1
	btfss INDF0, 1, 0
	call ENVIA_0
	btfsc INDF0, 1, 0
	call ENVIA_1
	btfss INDF0, 0, 0
	call ENVIA_0
	btfsc INDF0, 0, 0
	call ENVIA_1
	;aumentamos el contador de bits enviados
	incf ENVIADOS, 1, 0
	movf TABLAT, 0, 0
	cpfslt ENVIADOS, 0
	;si toca encender LEDS de la barra de progreso se encendera
	call PROGRESS_BAR
	;comprobamos si hemos llegado al final del mensaje
	movlw 0x00
	cpfsgt INDF0, 0
	goto ENVIA_FINAL
	;si todavia queda mensaje leeremos el siguiente caracter de mensaje
	movf POSTINC0, 0, 0 ;hay que hacer algo con el POSTINC0 para que aumente su valor
	goto SIGUE_ENVIANDO
;ponemos a '1' la salida RF
ENVIA_FINAL	
	bsf LATC, 0, 0
	;clrf HAY_MENSAJE, 0 ULLLLLLLL
	setf BLINKING_10HZ, 0 ;indicar que habra que hacer blinking a 10Hz
	return

;enviamos un '1' en manchester
ENVIA_1
	clrf TIEMPOL, 0
	call RESET_TIMER
	;ponemos la salida a '0' y esperamos 5ms
	bcf LATC, 0, 0
WAIT_5MS_1
	btfss TIEMPOL, 0, 0
	goto WAIT_5MS_1
	;cuando pasan los 5ms ponemos la salida a '1' y volvemos a esperar
	bsf LATC, 0, 0
	clrf TIEMPOL, 0
WAIT_5MS_2
	btfss TIEMPOL, 0, 0
	goto WAIT_5MS_2
	return
	
;enviamos un '0' en manchester	
ENVIA_0
	clrf TIEMPOL, 0
	call RESET_TIMER
	;ponemos la salida a '1' y esperamos 5ms
	bsf LATC, 0, 0
WAIT_5MS_3
	btfss TIEMPOL, 0, 0
	goto WAIT_5MS_3
	;cuando pasan los 5ms ponemos la salida a '0' y volvemos a esperar
	bcf LATC, 0, 0
	clrf TIEMPOL, 0
WAIT_5MS_4
	btfss TIEMPOL, 0, 0
	goto WAIT_5MS_4
	return	
	
;si han pasado 100ms (5Hz) invertimos los LEDS	
BLINKING_5
	movlw .19
	cpfsgt TIEMPOL, 0
	return
	clrf TIEMPOL, 0
	comf LATD, 1, 0
	comf LATE, 1, 0
	return
	
;si han pasado 50ms (10Hz) invertimos los LEDS		
BLINKING_10
	movlw 0x04
	cpfsgt TIEMPOL, 0
	return
	clrf TIEMPOL, 0
	comf LATD, 1, 0
	comf LATE, 1, 0
	return
	
ENCENDER_TODO
	setf LATD, 0
	setf LATE, 0
	return
	
;rotamos los leds que teniamos como progreso y encendemos uno mas	
PROGRESS_BAR
	clrf ENVIADOS, 0
	rlcf LATD, 1, 0
	rlcf LATE, 1, 0
	bsf LATD, 0, 0
	return
	
;*******
;* END *
;*******
	END