package org.p2f1.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import org.p2f1.models.MainWindowModel;
import org.p2f1.views.MainWindowView;
import org.p2f1.models.LecturaPlaca;

import com.SerialPort.SerialPort;

public class MainWindowController implements ActionListener, ItemListener {

	private MainWindowView view = null;
	private MainWindowModel model = null;
	private LecturaPlaca lectura = null;
	
	public MainWindowController(MainWindowView view, MainWindowModel model){
		try{
			this.view = view;
			this.model = model;
			this.view.associateController(this);
			lectura = new LecturaPlaca(view, model);
			lectura.start();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton){
			JButton btn = (JButton) e.getSource();
			switch(btn.getName()){
				case MainWindowView.BTN_RF:
					if (lectura.enviaRF()) {
						JOptionPane.showMessageDialog(null, "Ha empezado la transmision RF!",
								"MESSAGE LOADED", JOptionPane.INFORMATION_MESSAGE);
					}
					break;
				case MainWindowView.BTN_UART:
					//CODIGO TRANSIMISON EUSART
					if (lectura.enviaMensaje()) {
						JOptionPane.showMessageDialog(null, "El mensaje se ha cargado en la PIC!",
								"MESSAGE LOADED", JOptionPane.INFORMATION_MESSAGE);
						view.clrText();
						view.clrChars();
					}
					break;
			}
		}
	}
	
	public void showView(){
		view.setVisible(true);
	}

    @Override
    public void itemStateChanged(ItemEvent e) {
	    if (e == null) {
	        return;
        }
        if (e.getStateChange() == ItemEvent.SELECTED) {
            String port = view.getPort();
            Integer baud = view.getBaud();
            if (port == null || lectura == null)
                return;
            lectura.actualizaPuerto(port, baud);
        }
    }
}