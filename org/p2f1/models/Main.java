package org.p2f1.models;

import org.p2f1.controllers.MainWindowController;
import org.p2f1.models.MainWindowModel;
import org.p2f1.views.MainWindowView;

import javax.swing.*;

public class Main {

	public static void main(String [] args){
		SwingUtilities.invokeLater(() -> {
			MainWindowView view = new MainWindowView();
			MainWindowModel model = new MainWindowModel();
			MainWindowController controller = new MainWindowController(view, model);
			controller.showView();

		});
	}

}