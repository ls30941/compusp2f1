package org.p2f1.models;

import com.SerialPort.SerialPort;
import org.p2f1.views.MainWindowView;

import javax.swing.*;


/**
 * Created by alejandra on 20/02/2017.
 */
public class LecturaPlaca extends Thread {

    //private static final int BAUD_RATE = 19200;
    private SerialPort sp = null;
    private MainWindowView view = null;
    private MainWindowModel model = null;
    private boolean isOpen = false;

    public LecturaPlaca(MainWindowView view, MainWindowModel model) {
        this.view = view;
        this.model = model;
        try {
            sp = new SerialPort();
            view.setBaudRateList(sp.getAvailableBaudRates());
            view.setPortsList(sp.getPortList());
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            sleep(100);
        } catch (Exception e) {}
        while (!view.isClosed()) {
            try {
                sleep(3);
            } catch (Exception e) {}
            while (isOpen) {
                try {
                    sleep(3);
                } catch (Exception e) {}
                try {
                    byte b = sp.readByte();
                    if (b == (byte) 'A') {
                        if (enviaMensaje()) {
                            JOptionPane.showMessageDialog(null, "El mensaje se ha cargado en la PIC!",
                                    "MESSAGE LOADED", JOptionPane.INFORMATION_MESSAGE);
                            view.clrText();
                            view.clrChars();
                        }
                    }
                } catch (Exception e2) {
                    System.out.println("han pasado 5 segs");
                }
                if (view.isClosed()) isOpen = false;
            }
        }
        try {
            sp.closePort();
        } catch(Exception e2) {}
    }

    public boolean openPort() {

        try {
            System.out.println(view.getPort());
            System.out.println(view.getBaud());
            sp.openPort(view.getPort(), view.getBaud());
            return true;
        } catch (Exception e1) {
            JOptionPane.showMessageDialog(null, "No se ha detectado el puerto.",
                    "PORT ERROR", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    public void closePort(){
        sp.closePort();
    }

    public boolean enviaMensaje() {
        if (!model.checkInputText(view.getText()) || !model.checkBaudRate(view.getBaud()) || !isOpen) {
            if (view.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "No hay mensaje a enviar!",
                        "Message ERROR", JOptionPane.ERROR_MESSAGE);
            } else if (view.getText().length() > 301) {
                JOptionPane.showMessageDialog(null, "El mensaje no puede superar los 300 caracteres!",
                        "MESSAGE ERROR", JOptionPane.ERROR_MESSAGE);
            } else if (!model.checkBaudRate(view.getBaud())){
                JOptionPane.showMessageDialog(null, "La pic esta programada para trabajar con un baudrate de 19200!",
                        "MESSAGE ERROR", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "El canal serie no esta abierto!",
                        "MESSAGE ERROR", JOptionPane.ERROR_MESSAGE);
            }
            return false;
        } else {
            try {
                sp.writeByte((byte) '.');
                for (char c : view.getText().toCharArray()) {
                    sp.writeByte((byte) c);
                    System.out.println((byte) c + " -> " + c);
                }
                sp.writeByte((byte) 0);
                return true;
            } catch(Exception e){
                return false;
            }
        }
    }

    public void actualizaPuerto(String puerto, int boudrate) {
        try {
            if (isOpen) {
                isOpen = false;
                closePort();
                System.out.println("TANCAT");
            }
            if (openPort()) {
                isOpen = true;
                System.out.println("Obert");
            }
        } catch (Exception ex) {

        }
    }

    public boolean enviaRF() {
        try {
            sp.writeByte((byte) 'D');
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido enviar la comanda a la placa!",
                    "Message ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}

